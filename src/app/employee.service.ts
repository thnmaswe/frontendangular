import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from "@angular/common/http";
import {catchError, map, Observable, retry, throwError} from "rxjs";
import {Department} from "./department";
import {Employee} from "./employee";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  baseUrl = 'http://localhost:8082';

  constructor(private http: HttpClient) { }

  getDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(this.baseUrl+'/departments')
      .pipe(
        retry(1),
        map((d:any) => {return d._embedded.departments}),
        catchError(this.handleError)
      )
  }

  getEmployees(department : string|null = null): Observable<Employee[]> {
    const options = department ?
      {params: new HttpParams().set('id', department)} : {};
    const url = department ? this.baseUrl+'/employees/search/department' : this.baseUrl+'/employees';
    return this.http.get<Employee[]>(url, options)
      .pipe(
        retry(1),
        map((d: any) => {return d._embedded.employees}),
        catchError(this.handleError)
      )
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage: string;
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    console.log(errorMessage);
    return throwError(() => new Error(errorMessage));
  }
}
