import {Component, OnInit} from '@angular/core';
import {Employee} from "../employee";
import {Department} from "../department";
import {EmployeeService} from "../employee.service";
import {MatFormField, MatLabel, MatOption, MatSelect, MatSelectChange} from "@angular/material/select";
import {
  MatCell, MatCellDef,
  MatColumnDef,
  MatHeaderCell,
  MatHeaderCellDef,
  MatHeaderRow, MatHeaderRowDef,
  MatRow, MatRowDef,
  MatTable,
} from "@angular/material/table";
import {MatSort, MatSortHeader} from "@angular/material/sort";
import {NgForOf} from "@angular/common";

@Component({
  selector: 'app-employees',
  standalone: true,
  imports: [
    MatSelect,
    MatOption,
    MatTable,
    MatHeaderCell,
    MatCell,
    MatHeaderRow,
    MatRow,
    MatSort,
    MatColumnDef,
    MatSortHeader,
    NgForOf,
    MatHeaderCellDef,
    MatCellDef,
    MatHeaderRowDef,
    MatRowDef,
    MatFormField,
    MatLabel
  ],
  templateUrl: './employees.component.html',
  styleUrl: './employees.component.css'
})
export class EmployeesComponent implements OnInit{
  ds: Array<Employee> = new Array<Employee>();
  displayedColumns: string[] = ['surname', 'givenname', 'email', 'phoneNumber'];

  departments: Array<Department> = new Array<Department>();
  selectedDepartment: string | null = null;

  constructor(private empService : EmployeeService) { }

  ngOnInit() {
    this.empService.getDepartments().subscribe(res => {this.departments = res})
    this.empService.getEmployees(null).subscribe(res  =>  {this.ds = res})
  }

  onDepartmentSelected(changeEvent: MatSelectChange) {
    this.empService.getEmployees(changeEvent.value).subscribe(res => {this.ds = res})
  }
}
